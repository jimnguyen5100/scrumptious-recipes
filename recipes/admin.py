from django.contrib import admin


from.models import Recipe

admin.site.register(Recipe)

from.models import Measure

admin.site.register(Measure)

from.models import FoodItem

admin.site.register(FoodItem)

from.models import Ingredient

admin.site.register(Ingredient)

from.models import Step

admin.site.register(Step)
