from django.db import models


class Recipe(models.Model):
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.name + " by " + self.author

class Measure(models.Model):
    name = models.CharField(max_length=30, unique =True)
    abbreviation = models.CharField(max_length=10, unique=True)
    def __str__(self):
        return self.name

class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)
    def __str__(self):
        return self.name

class Ingredient(models.Model):
    Recipe = models.ForeignKey(
        Recipe,
        on_delete=models.CASCADE,
    )

    Measure = models.ForeignKey(
        Measure,
        on_delete=models.PROTECT,
    )

    FoodItem = models.ForeignKey(
        FoodItem,
        on_delete=models.PROTECT,
    )

    Amount = models.FloatField()

    def __str__(self):
        return self.FoodItem.name


class Step(models.Model):
    recipe = models.ForeignKey(Recipe, related_name="steps", on_delete=models.CASCADE)
    order = models.SmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", null=True, blank=True)

    def __str__(self):
        return self.recipe.name
